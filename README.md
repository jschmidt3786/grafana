Bastille Template to create a Grafana Jail

## Status
[![pipeline status](https://gitlab.com/bastillebsd-templates/grafana/badges/master/pipeline.svg)](https://gitlab.com/bastillebsd-templates/grafana/commits/master)

## Bootstrap

```shell
ishmael ~ # bastille bootstrap https://github.com/bastillebsd-templates/grafana
```

## Usage

```shell
ishmael ~ # bastille template TARGET bastillebsd-templates/grafana
```
